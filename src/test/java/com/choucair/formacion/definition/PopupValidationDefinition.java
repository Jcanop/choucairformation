package com.choucair.formacion.definition;

import com.choucair.formacion.steps.PopupValidationSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {
	
	@Steps
	PopupValidationSteps popupValidationSteps;
	
	@Given("^Autentico el colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_el_colorlib_con_usuario_y_clave(String strUsuario, String strPass) throws Throwable {
	popupValidationSteps.login_colorlib(strUsuario, strPass);
	}

	@Given("^Ingreso a la funcionalidad Forms validation$")
	public void ingreso_a_la_funcionalidad_Forms_validation() throws Throwable {
		popupValidationSteps.ingresar_form_valadiation();
	}

	@When("^Diligencio Formulario Popup Validation$")
	public void diligencio_Formulario_Popup_Validation() throws Throwable {

	}

	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso() throws Throwable {

	}


}
